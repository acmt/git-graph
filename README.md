# Git Graph

Render beautiful Git graphs from Typescript/Javascript/Node or directly from Git :tada:. Exports to HTMLElement (via JS/TS), SVG, PNG and JPG (via Git).